# ProjetCookBook

Projet d'approfondissement en programmation. Application de cuisine.

Aucune installation requise pour le fonctionnement de l'application. Une vidéo tutorielle est intégrée pour faciliter l'utilisation de l'application.
Les données sont sauvegardées dans les fichiers du programme.

L'application aura des problèmes si vous supprimez les éléments déjà présents dans les fichiers de l'application, incluant les images, sons et icônes.

Lors du clic d'un bouton, un effet sonore est ajouté.

Finalement, pour débuter l'utilisation, simplement dézipper le fichier "release" et utiliser le .exe à l'intérieur.
