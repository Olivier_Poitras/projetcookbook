﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmVideoExplicative : Form
    {
        #region Attributs

        private string _cheminVideo;
        private string _cheminMaVideo;

        #endregion

        public frmVideoExplicative()
        {
            InitializeComponent();
            wmpMaVideo.URL = _cheminMaVideo;
            string videoURL = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "tutorielOP.mp4");
            if (!File.Exists(videoURL))
            {
                File.WriteAllBytes(videoURL, ProjetApprofondissement.Properties.Resources.tutorielOP);
            }
            wmpMaVideo.uiMode = "none";
            wmpMaVideo.URL = videoURL;
            wmpMaVideo.Ctlcontrols.play();
        }

        #region Gestion des boutons

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            wmpMaVideo.Ctlcontrols.play();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            wmpMaVideo.Ctlcontrols.pause();
        }

        #endregion

        #region Gestion de la fenêtre

        private void frmVideoExplicative_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner.Show();
            this.Dispose();
        }

        private void frmVideoExplicative_Load(object sender, EventArgs e)
        {
            btnExit.Left = this.ClientSize.Width - 70;
        }

        #endregion
    }
}
