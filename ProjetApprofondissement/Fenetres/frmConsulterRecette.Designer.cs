﻿
namespace ProjetApprofondissement.Fenetres
{
    partial class frmConsulterRecette
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsulterRecette));
            this.listboxEtapes = new System.Windows.Forms.ListBox();
            this.listboxIngredients = new System.Windows.Forms.ListBox();
            this.lblEtapes = new System.Windows.Forms.Label();
            this.lblIngredients = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblTemps = new System.Windows.Forms.Label();
            this.txtTemps = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtCalories = new System.Windows.Forms.TextBox();
            this.lblCalories = new System.Windows.Forms.Label();
            this.lblNbConvives = new System.Windows.Forms.Label();
            this.numNbConvives = new System.Windows.Forms.NumericUpDown();
            this.btnRecalcul = new System.Windows.Forms.Button();
            this.lblNomRecette = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.picRecette = new System.Windows.Forms.PictureBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.numNbConvives)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRecette)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // listboxEtapes
            // 
            this.listboxEtapes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.listboxEtapes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listboxEtapes.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.listboxEtapes.ForeColor = System.Drawing.Color.White;
            this.listboxEtapes.FormattingEnabled = true;
            this.listboxEtapes.HorizontalScrollbar = true;
            this.listboxEtapes.ItemHeight = 23;
            this.listboxEtapes.Location = new System.Drawing.Point(1229, 139);
            this.listboxEtapes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listboxEtapes.Name = "listboxEtapes";
            this.listboxEtapes.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listboxEtapes.Size = new System.Drawing.Size(687, 552);
            this.listboxEtapes.TabIndex = 1;
            // 
            // listboxIngredients
            // 
            this.listboxIngredients.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.listboxIngredients.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listboxIngredients.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.listboxIngredients.ForeColor = System.Drawing.Color.White;
            this.listboxIngredients.FormattingEnabled = true;
            this.listboxIngredients.ItemHeight = 23;
            this.listboxIngredients.Location = new System.Drawing.Point(144, 139);
            this.listboxIngredients.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listboxIngredients.Name = "listboxIngredients";
            this.listboxIngredients.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listboxIngredients.Size = new System.Drawing.Size(351, 552);
            this.listboxIngredients.TabIndex = 2;
            // 
            // lblEtapes
            // 
            this.lblEtapes.AutoSize = true;
            this.lblEtapes.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEtapes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblEtapes.Location = new System.Drawing.Point(1221, 83);
            this.lblEtapes.Name = "lblEtapes";
            this.lblEtapes.Size = new System.Drawing.Size(147, 39);
            this.lblEtapes.TabIndex = 3;
            this.lblEtapes.Text = "Étapes :";
            // 
            // lblIngredients
            // 
            this.lblIngredients.AutoSize = true;
            this.lblIngredients.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIngredients.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblIngredients.Location = new System.Drawing.Point(137, 83);
            this.lblIngredients.Name = "lblIngredients";
            this.lblIngredients.Size = new System.Drawing.Size(234, 39);
            this.lblIngredients.TabIndex = 4;
            this.lblIngredients.Text = "Ingrédients :";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.txtDescription.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtDescription.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.ForeColor = System.Drawing.Color.White;
            this.txtDescription.Location = new System.Drawing.Point(204, 805);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ReadOnly = true;
            this.txtDescription.Size = new System.Drawing.Size(512, 130);
            this.txtDescription.TabIndex = 6;
            // 
            // lblTemps
            // 
            this.lblTemps.AutoSize = true;
            this.lblTemps.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemps.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblTemps.Location = new System.Drawing.Point(774, 803);
            this.lblTemps.Name = "lblTemps";
            this.lblTemps.Size = new System.Drawing.Size(502, 27);
            this.lblTemps.TabIndex = 7;
            this.lblTemps.Text = "Temps requis pour compléter la recette :\r\n";
            // 
            // txtTemps
            // 
            this.txtTemps.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.txtTemps.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTemps.Font = new System.Drawing.Font("Kristen ITC", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTemps.ForeColor = System.Drawing.Color.White;
            this.txtTemps.Location = new System.Drawing.Point(1314, 803);
            this.txtTemps.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtTemps.Name = "txtTemps";
            this.txtTemps.ReadOnly = true;
            this.txtTemps.Size = new System.Drawing.Size(101, 18);
            this.txtTemps.TabIndex = 8;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Lucida Bright", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblDescription.Location = new System.Drawing.Point(200, 765);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(209, 34);
            this.lblDescription.TabIndex = 9;
            this.lblDescription.Text = "Description :";
            // 
            // txtCalories
            // 
            this.txtCalories.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.txtCalories.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalories.Font = new System.Drawing.Font("Kristen ITC", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalories.ForeColor = System.Drawing.Color.White;
            this.txtCalories.Location = new System.Drawing.Point(1314, 874);
            this.txtCalories.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtCalories.Name = "txtCalories";
            this.txtCalories.ReadOnly = true;
            this.txtCalories.Size = new System.Drawing.Size(101, 18);
            this.txtCalories.TabIndex = 11;
            // 
            // lblCalories
            // 
            this.lblCalories.AutoSize = true;
            this.lblCalories.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCalories.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblCalories.Location = new System.Drawing.Point(911, 869);
            this.lblCalories.Name = "lblCalories";
            this.lblCalories.Size = new System.Drawing.Size(365, 27);
            this.lblCalories.TabIndex = 10;
            this.lblCalories.Text = "Calories totales de la recette :\r\n";
            // 
            // lblNbConvives
            // 
            this.lblNbConvives.AutoSize = true;
            this.lblNbConvives.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbConvives.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblNbConvives.Location = new System.Drawing.Point(1481, 803);
            this.lblNbConvives.Name = "lblNbConvives";
            this.lblNbConvives.Size = new System.Drawing.Size(276, 27);
            this.lblNbConvives.TabIndex = 13;
            this.lblNbConvives.Text = "Nombre de convives :";
            // 
            // numNbConvives
            // 
            this.numNbConvives.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.numNbConvives.ForeColor = System.Drawing.Color.White;
            this.numNbConvives.Location = new System.Drawing.Point(1773, 803);
            this.numNbConvives.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.numNbConvives.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numNbConvives.Name = "numNbConvives";
            this.numNbConvives.Size = new System.Drawing.Size(120, 22);
            this.numNbConvives.TabIndex = 15;
            this.numNbConvives.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnRecalcul
            // 
            this.btnRecalcul.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRecalcul.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnRecalcul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecalcul.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRecalcul.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnRecalcul.Location = new System.Drawing.Point(1531, 846);
            this.btnRecalcul.Margin = new System.Windows.Forms.Padding(4);
            this.btnRecalcul.Name = "btnRecalcul";
            this.btnRecalcul.Size = new System.Drawing.Size(364, 71);
            this.btnRecalcul.TabIndex = 16;
            this.btnRecalcul.Text = "Recalcul des quantités";
            this.btnRecalcul.UseVisualStyleBackColor = true;
            this.btnRecalcul.Click += new System.EventHandler(this.btnRecalcul_click);
            // 
            // lblNomRecette
            // 
            this.lblNomRecette.AutoSize = true;
            this.lblNomRecette.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.lblNomRecette.Font = new System.Drawing.Font("Lucida Bright", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomRecette.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblNomRecette.Location = new System.Drawing.Point(831, -1);
            this.lblNomRecette.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNomRecette.Name = "lblNomRecette";
            this.lblNomRecette.Size = new System.Drawing.Size(447, 53);
            this.lblNomRecette.TabIndex = 20;
            this.lblNomRecette.Text = "Nom de la recette";
            this.lblNomRecette.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.pictureBox3.Location = new System.Drawing.Point(1451, 729);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(3, 250);
            this.pictureBox3.TabIndex = 31;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.pictureBox1.Location = new System.Drawing.Point(753, 729);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(3, 250);
            this.pictureBox1.TabIndex = 30;
            this.pictureBox1.TabStop = false;
            // 
            // picRecette
            // 
            this.picRecette.Image = global::ProjetApprofondissement.Properties.Resources.cookingDefault;
            this.picRecette.Location = new System.Drawing.Point(514, 139);
            this.picRecette.MaximumSize = new System.Drawing.Size(700, 520);
            this.picRecette.Name = "picRecette";
            this.picRecette.Size = new System.Drawing.Size(700, 520);
            this.picRecette.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picRecette.TabIndex = 21;
            this.picRecette.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::ProjetApprofondissement.Properties.Resources.back32;
            this.btnClose.Location = new System.Drawing.Point(16, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(44, 41);
            this.btnClose.TabIndex = 18;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.pictureBox2.Location = new System.Drawing.Point(-7, -4);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(4000, 66);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "rectangleDarkRed";
            // 
            // frmConsulterRecette
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(73)))));
            this.ClientSize = new System.Drawing.Size(2244, 1014);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.picRecette);
            this.Controls.Add(this.lblNomRecette);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnRecalcul);
            this.Controls.Add(this.numNbConvives);
            this.Controls.Add(this.lblNbConvives);
            this.Controls.Add(this.txtCalories);
            this.Controls.Add(this.lblCalories);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.txtTemps);
            this.Controls.Add(this.lblTemps);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblIngredients);
            this.Controls.Add(this.lblEtapes);
            this.Controls.Add(this.listboxIngredients);
            this.Controls.Add(this.listboxEtapes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "frmConsulterRecette";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultation de votre recette.";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmConsulterRecette_FormClosing);
            this.Load += new System.EventHandler(this.frmConsulterRecette_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numNbConvives)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRecette)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listboxEtapes;
        private System.Windows.Forms.ListBox listboxIngredients;
        private System.Windows.Forms.Label lblEtapes;
        private System.Windows.Forms.Label lblIngredients;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblTemps;
        private System.Windows.Forms.TextBox txtTemps;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtCalories;
        private System.Windows.Forms.Label lblCalories;
        private System.Windows.Forms.Label lblNbConvives;
        private System.Windows.Forms.NumericUpDown numNbConvives;
        private System.Windows.Forms.Button btnRecalcul;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblNomRecette;
        private System.Windows.Forms.PictureBox picRecette;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}