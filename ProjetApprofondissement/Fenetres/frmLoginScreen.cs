﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmLoginScreen : Form
    {
        #region Attributs

        private Dictionary<string, Utilisateur> _dicoUsers;
        private Dictionary<string, byte[]> _dicoSalts;
        private List<Ingredient> _listeIngredients;

        #endregion

        #region Accesseurs

        public Dictionary<string, Utilisateur> DicoUsers
        {
            get { return _dicoUsers; }
            set { _dicoUsers = value; }
        }

        public Dictionary<string, byte[]> DicoSalts
        {
            get { return _dicoSalts; }
            set { _dicoSalts = value; }
        }

        public List<Ingredient> ListeIngredients
        {
            get { return _listeIngredients; }
            set { _listeIngredients = value; }
        }

        #endregion

        public frmLoginScreen()
        {
            InitializeComponent();
            ChargerUtilisateurs();
            ChargerIngredients();
            ChargerSalts();
        }

        /// <summary>
        /// Cette méthode charge les comptes utilisateurs existants à l'ouverture de l'application.
        /// </summary>
        private void ChargerUtilisateurs()
        {
            if (File.Exists("users.save"))
            {
                DataSerializer dataSerializer = new DataSerializer();
                DicoUsers = (Dictionary<string, Utilisateur>)dataSerializer.JsonDeserialize(typeof(Dictionary<string, Utilisateur>), "users.save");
            }
            else
            {
                DicoUsers = new Dictionary<string, Utilisateur>();
            }
        }

        /// <summary>
        /// Cette méthode charge les salts (pour les mots de passe) à l'ouverture de l'application.
        /// </summary>
        private void ChargerSalts()
        {
            if (File.Exists("salts.save"))
            {
                DataSerializer dataSerializer = new DataSerializer();
                DicoSalts = (Dictionary<string, byte[]>)dataSerializer.JsonDeserialize(typeof(Dictionary<string, byte[]>), "salts.save");
            }
            else
            {
                DicoSalts = new Dictionary<string, byte[]>();
            }
        }

        /// <summary>
        /// Cette méthode charge les ingrédients à l'ouverture de l'application.
        /// Lors de la première utilisation de l'application, le programme insère
        /// quelques ingrédients.
        /// </summary>
        private void ChargerIngredients()
        {
            if (File.Exists("ingredients.save"))
            {
                DataSerializer dataSerializer = new DataSerializer();
                using (StreamReader sr = new StreamReader("ingredients.save"))
                {
                    if (sr.ReadToEnd() != "[]")
                        ListeIngredients =
                            (List<Ingredient>)dataSerializer.JsonDeserialize(typeof(List<Ingredient>),
                                "ingredients.save");
                    else ListeIngredients = new List<Ingredient>();
                }
            }
            else
            {
                ListeIngredients = new List<Ingredient>();
                ListeIngredients.Add(new Ingredient("Pâte à pizza", "portions", 3, 350));
                ListeIngredients.Add(new Ingredient("Fromage", "bloc", 1, 200));
                ListeIngredients.Add(new Ingredient("Sauce tomate", "cannes", 1, 75));
                ListeIngredients.Add(new Ingredient("Pepperoni", "tranches", 10, 140));
                ListeIngredients.Add(new Ingredient("Macaroni", "grammes", 300, 250));
                ListeIngredients.Add(new Ingredient("Olives vertes", "unités", 15, 55));
                ListeIngredients.Add(new Ingredient("Cornichons", "unités", 5, 70));
                ListeIngredients.Add(new Ingredient("Mayonnaise", "cuillères à table", 3, 100));
                ListeIngredients.Add(new Ingredient("Poulet", "portions", 2, 150));
            }
        }

        #region Gestion des boutons

        private void btnConnexion_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Hide();
            frmConnexion frmConnexion = new frmConnexion(this);
            frmConnexion.Owner = this;
            frmConnexion.Show();
        }

        private void btnInscription_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Hide();
            frmInscription frmInscripton = new frmInscription(this);
            frmInscripton.Owner = this;
            frmInscripton.Show();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            DataSerializer dataSerializer = new DataSerializer();
            dataSerializer.JsonSerialize(DicoUsers, "users.save");
            dataSerializer.JsonSerialize(ListeIngredients, "ingredients.save");
            dataSerializer.JsonSerialize(DicoSalts, "salts.save");
            this.Dispose();
        }

        private void btnVideo_MouseEnter(object sender, EventArgs e)
        {
            btnVideo.ForeColor = Color.FromArgb(0, 126, 249);
        }

        private void btnExit_MouseEnter(object sender, EventArgs e)
        {
            btnExit.ForeColor = Color.FromArgb(0, 126, 249);
        }

        private void btnVideo_MouseLeave(object sender, EventArgs e)
        {
            btnVideo.ForeColor = Color.FromArgb(24, 30, 54);
        }

        private void btnExit_MouseLeave(object sender, EventArgs e)
        {
            btnExit.ForeColor = Color.FromArgb(24, 30, 54);
        }

        private void btnVideo_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Hide();
            frmVideoExplicative frmVideoExplicative = new frmVideoExplicative();
            frmVideoExplicative.Owner = this;
            frmVideoExplicative.Show();
        }

        #endregion
    }
}
