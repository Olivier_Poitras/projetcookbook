﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmConnexion : Form
    {
        #region Attributs

        private frmLoginScreen _mainForm = null;

        #endregion

        #region Accesseurs

        public frmLoginScreen MainForm
        {
            get { return _mainForm; }
            set { _mainForm = value; }
        }

        #endregion

        public frmConnexion()
        {
            InitializeComponent();
        }

        public frmConnexion(Form callingForm)
        {
            MainForm = callingForm as frmLoginScreen;
            InitializeComponent();
        }

        #region Gestion des boutons

        private void btnConnexion_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            errorProvider1.Clear();
            if (MainForm.DicoUsers.ContainsKey(txtIdentifiant.Text))
            {
                if (Utilitaires.VerifierMdp(txtMdp.Text, MainForm.DicoSalts[txtIdentifiant.Text],
                    MainForm.DicoUsers[txtIdentifiant.Text].MotDePasse))
                {
                    frmHomePage frmHomePage = new frmHomePage(this.MainForm, txtIdentifiant.Text);
                    frmHomePage.Owner = this.MainForm;
                    frmHomePage.Show();
                    this.Dispose();
                }
                else
                {
                    errorProvider1.SetError(txtMdp, "Mot de passe incorrect.");
                }
            }
            else
            {
                errorProvider1.SetError(txtIdentifiant, "Identifiant non-existant.");
            }
        }

        private void btnAnnuler_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Owner.Show();
            this.Dispose();
        }

        #endregion

        #region Gestion de l'accessibilité

        private void txtIdentifiant_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        private void txtMdp_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SendKeys.Send("{TAB}");
            }
        }

        #endregion
    }
}
