﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmAfficherIngredients : Form
    {
        #region Attributs

        private frmLoginScreen _mainForm = null;

        #endregion

        #region Accesseurs

        public frmLoginScreen MainForm
        {
            get { return _mainForm; }
            set { _mainForm = value; }
        }

        #endregion

        public frmAfficherIngredients()
        {
            InitializeComponent();
        }

        public frmAfficherIngredients(Form callingForm)
        {
            MainForm = callingForm as frmLoginScreen;
            InitializeComponent();
        }

        #region Gestion de la fenêtre

        private void frmAfficherIngredients_Load(object sender, EventArgs e)
        {
            AfficherIngredients();
        }

        private void frmAfficherIngredients_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner.Show();
            this.Dispose();
        }

        #endregion

        #region Gestion des boutons

        private void btnClose_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Owner.Show();
            this.Dispose();
        }

        private void btnSupprimerIngredient_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            if (listboxIngredients.SelectedIndex >= 0 &&
                listboxIngredients.SelectedIndex < MainForm.ListeIngredients.Count)
            {
                MainForm.ListeIngredients.RemoveAt(listboxIngredients.SelectedIndex);
                listboxIngredients.Items.Remove(listboxIngredients.SelectedItem);
            }
            AfficherIngredients();
        }

        private void btnCreerIngredient_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            frmCreerIngredient frmCreerIngredient = new frmCreerIngredient(this);
            frmCreerIngredient.Owner = this;
            this.Hide();
            frmCreerIngredient.Show();
        }

        #endregion

        /// <summary>
        /// Cette méthode affiche les ingrédients dans la liste de la fenêtre.
        /// Ces ingrédients sont communs à tous les utilisateurs.
        /// </summary>
        private void AfficherIngredients()
        {
            listboxIngredients.Items.Clear();
            if (MainForm.ListeIngredients != null)
            {
                foreach (Ingredient ingredient in MainForm.ListeIngredients)
                {
                    listboxIngredients.Items.Add($"{ingredient.Nom} | {ingredient.MesureFixe} {ingredient.MesureUtilisee}");
                }
            }
        }

        /// <summary>
        /// Cette méthode crée un ingrédient à partir d'un ingrédient reçu en
        /// paramètre. La liste d'ingrédients est mise à jour.
        /// </summary>
        /// <param name="pIngredient"></param>
        public void CreerIngredient(Ingredient pIngredient)
        {
            pIngredient.QuantiteRequiseInitiale = pIngredient.QuantiteRequise;
            MainForm.ListeIngredients.Add(pIngredient);
            listboxIngredients.Items.Clear();
            AfficherIngredients();
        }
    }
}
