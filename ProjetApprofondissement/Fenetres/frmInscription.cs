﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmInscription : Form
    {
        private frmLoginScreen _mainForm = null;

        public frmLoginScreen MainForm
        {
            get { return _mainForm; }
            set { _mainForm = value; }
        }

        public frmInscription()
        {
            InitializeComponent();
        }

        public frmInscription(Form callingForm)
        {
            MainForm = callingForm as frmLoginScreen;
            InitializeComponent();
        }

        #region Gestion des boutons

        private void btnInscription_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            errorProvider1.Clear();
            if (MainForm.DicoUsers.ContainsKey(txtIdentifiant.Text))
            {
                errorProvider1.SetError(txtIdentifiant, "Identifiant déjà existant.");
            }
            else
            {
                MainForm.DicoSalts.Add(txtIdentifiant.Text, Utilitaires.SaltMotDePasse());
                Utilisateur user = new Utilisateur(txtIdentifiant.Text,
                    Utilitaires.HashMotDePasse(txtMdp.Text, MainForm.DicoSalts[txtIdentifiant.Text]));
                MainForm.DicoUsers.Add(txtIdentifiant.Text, user);
                MessageBox.Show("Le compte a été créé. Vous pouvez maintenant vous connecter.");
                // Insertion de recettes
                List<string> e1 = new List<string>();
                e1.Add("Faire bouillir l'eau.");
                e1.Add("Pendant ce temps, préparer un mélange d'épices désiré.");
                e1.Add("Lorsque l'eau est portée à ébullition, rajouter le macaroni.");
                e1.Add("Sortir la mayonnaise, les olives, les cornichons et le poulet du réfrigérateur.");
                e1.Add("Couper les cornichons et le poulet en tranches de grandeur désirée.");
                e1.Add("Lorsque prêt, rincer le macaroni à l'eau froide.");
                e1.Add("Dans un gros bol, mélanger tous les ingrédients.");
                e1.Add("Servir.");
                List<Ingredient> i1 = new List<Ingredient>();
                i1.Add(new Ingredient("Macaroni", "grammes", 250, 200));
                i1.Add(new Ingredient("Olives vertes", "unités", 15, 55));
                i1.Add(new Ingredient("Cornichons", "unités", 5, 70));
                i1.Add(new Ingredient("Mayonnaise", "cuillères à table", 3, 100));
                i1.Add(new Ingredient("Poulet", "portions", 2, 150));
                InsererRecette(txtIdentifiant.Text, "Salade de macaroni",
                    "Une déliciseuse salade de macaroni au poulet, olives vertes et cornichons.",
                    "30 min", e1, i1);

                List<string> e2 = new List<string>();
                e2.Add("Faire bouillir l'eau.");
                e2.Add("Pendant ce temps, couper le fromage en cubes.");
                e2.Add("Lorsque l'eau est portée à ébullition, rajouter le macaroni.");
                e2.Add("Ouvrir une canne de sauce tomate.");
                e2.Add("Lorsque prêt, égoutter le macaroni et remettre dans le chaudron.");
                e2.Add("Rajouter la sauce et mélanger.");
                e2.Add("En mélangeant, rajouter les cubes de fromage progressivement.");
                e2.Add("Lorsque le fromage est fondu, servir le repas.");
                List<Ingredient> i2 = new List<Ingredient>();
                i2.Add(new Ingredient("Macaroni", "grammes", 300, 250));
                i2.Add(new Ingredient("Fromage", "bloc", 1, 200));
                i2.Add(new Ingredient("Sauce tomate", "cannes", 2, 190));
                InsererRecette(txtIdentifiant.Text, "Macaroni au fromage",
                    "Un classique, le fameux Mac And Cheese!",
                    "25 min", e2, i2);

                List<string> e3 = new List<string>();
                e3.Add("Préchauffer le four.");
                e3.Add("Préparer et aplatir la pâte à pizza.");
                e3.Add("Étendre la sauce tomate sur la pâte.");
                e3.Add("Ajouter le fromage pour recouvrir toute la sauce.");
                e3.Add("Ajouter quelques tranches de pepperonni.");
                e3.Add("Cuire au four à 450 pendant 14 min.");
                e3.Add("Lorsque le fromage est doré, sortir du four.");
                e3.Add("Couper en pointes égales et servir le repas.");
                List<Ingredient> i3 = new List<Ingredient>();
                i3.Add(new Ingredient("Pâte à pizza", "portions", 3, 350));
                i3.Add(new Ingredient("Fromage", "bloc", 1, 200));
                i3.Add(new Ingredient("Sauce tomate", "cannes", 1, 75));
                i3.Add(new Ingredient("Pepperoni", "tranches", 10, 140));
                InsererRecette(txtIdentifiant.Text, "Pizza maison",
                    "Une pizza pepperonni-fromage.",
                    "45 min", e3, i3);


                this.Owner.Show();
                this.Dispose();
            }
        }

        private void btnAnnuler_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Owner.Show();
            this.Dispose();
        }

        #endregion

        /// <summary>
        /// Cette méthode insère une recette, selon les paramètres, à l'utilisateur concerné.
        /// </summary>
        /// <param name="key">L'identifiant de l'utilisateur actuel.</param>
        /// <param name="pNom">Le nom de la recette.</param>
        /// <param name="pDescription">La description de la recette.</param>
        /// <param name="pTemps">Le temps requis pour préparer la recette.</param>
        /// <param name="pEtapes">Les étapes de la recette.</param>
        /// <param name="pIngredients">Les ingrédients de la recette.</param>
        private void InsererRecette(string key, string pNom, string pDescription, string pTemps, List<string> pEtapes, List<Ingredient> pIngredients)
        {
            Recette maRecette = new Recette();
            maRecette.NbConvives = 1;
            maRecette.Nom = pNom;
            maRecette.Description = pDescription;
            maRecette.Temps = pTemps;
            maRecette.Etapes = pEtapes;
            maRecette.Ingredients = pIngredients;
            foreach (Ingredient ingredient in maRecette.Ingredients)
            {
                ingredient.QuantiteRequise = 1;
                ingredient.QuantiteRequiseInitiale = ingredient.QuantiteRequise;
            }
            maRecette.CalculerQuantitesEtCalories();
            MainForm.DicoUsers[key].Recettes.Add(maRecette);
        }
    }
}
