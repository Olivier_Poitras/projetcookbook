﻿
namespace ProjetApprofondissement.Fenetres
{
    partial class frmAfficherIngredients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAfficherIngredients));
            this.listboxIngredients = new System.Windows.Forms.ListBox();
            this.lblIngredients = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnSupprimerIngredient = new System.Windows.Forms.Button();
            this.btnCreerIngredient = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // listboxIngredients
            // 
            this.listboxIngredients.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.listboxIngredients.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listboxIngredients.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listboxIngredients.ForeColor = System.Drawing.Color.White;
            this.listboxIngredients.FormattingEnabled = true;
            this.listboxIngredients.ItemHeight = 23;
            this.listboxIngredients.Location = new System.Drawing.Point(86, 214);
            this.listboxIngredients.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listboxIngredients.Name = "listboxIngredients";
            this.listboxIngredients.Size = new System.Drawing.Size(759, 483);
            this.listboxIngredients.TabIndex = 10;
            this.listboxIngredients.Tag = "Center";
            // 
            // lblIngredients
            // 
            this.lblIngredients.AutoSize = true;
            this.lblIngredients.Font = new System.Drawing.Font("Lucida Bright", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIngredients.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblIngredients.Location = new System.Drawing.Point(333, 93);
            this.lblIngredients.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIngredients.Name = "lblIngredients";
            this.lblIngredients.Size = new System.Drawing.Size(393, 54);
            this.lblIngredients.TabIndex = 9;
            this.lblIngredients.Tag = "Center";
            this.lblIngredients.Text = "Les ingrédients :";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::ProjetApprofondissement.Properties.Resources.back32;
            this.btnClose.Location = new System.Drawing.Point(16, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(44, 41);
            this.btnClose.TabIndex = 14;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.pictureBox2.Location = new System.Drawing.Point(-7, -4);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(4000, 66);
            this.pictureBox2.TabIndex = 13;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "rectangleDarkRed";
            // 
            // btnSupprimerIngredient
            // 
            this.btnSupprimerIngredient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSupprimerIngredient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSupprimerIngredient.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupprimerIngredient.Image = global::ProjetApprofondissement.Properties.Resources.delete72;
            this.btnSupprimerIngredient.Location = new System.Drawing.Point(852, 483);
            this.btnSupprimerIngredient.Margin = new System.Windows.Forms.Padding(4);
            this.btnSupprimerIngredient.Name = "btnSupprimerIngredient";
            this.btnSupprimerIngredient.Size = new System.Drawing.Size(104, 96);
            this.btnSupprimerIngredient.TabIndex = 20;
            this.btnSupprimerIngredient.TabStop = false;
            this.btnSupprimerIngredient.Tag = "Center";
            this.btnSupprimerIngredient.UseVisualStyleBackColor = true;
            this.btnSupprimerIngredient.Click += new System.EventHandler(this.btnSupprimerIngredient_Click);
            // 
            // btnCreerIngredient
            // 
            this.btnCreerIngredient.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCreerIngredient.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreerIngredient.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreerIngredient.Image = global::ProjetApprofondissement.Properties.Resources.add72;
            this.btnCreerIngredient.Location = new System.Drawing.Point(852, 587);
            this.btnCreerIngredient.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreerIngredient.Name = "btnCreerIngredient";
            this.btnCreerIngredient.Size = new System.Drawing.Size(104, 96);
            this.btnCreerIngredient.TabIndex = 19;
            this.btnCreerIngredient.TabStop = false;
            this.btnCreerIngredient.Tag = "Center";
            this.btnCreerIngredient.UseVisualStyleBackColor = true;
            this.btnCreerIngredient.Click += new System.EventHandler(this.btnCreerIngredient_Click);
            // 
            // frmAfficherIngredients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(73)))));
            this.ClientSize = new System.Drawing.Size(1040, 780);
            this.Controls.Add(this.btnSupprimerIngredient);
            this.Controls.Add(this.btnCreerIngredient);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.listboxIngredients);
            this.Controls.Add(this.lblIngredients);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimizeBox = false;
            this.Name = "frmAfficherIngredients";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmAfficherIngredients";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAfficherIngredients_FormClosing);
            this.Load += new System.EventHandler(this.frmAfficherIngredients_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListBox listboxIngredients;
        private System.Windows.Forms.Label lblIngredients;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnSupprimerIngredient;
        private System.Windows.Forms.Button btnCreerIngredient;
    }
}