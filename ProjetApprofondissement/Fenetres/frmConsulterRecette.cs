﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmConsulterRecette : Form
    {
        #region Attributs

        private Recette laRecette;
        private Recette _laRecetteAffichee;

        #endregion

        #region Accesseurs

        public Recette LaRecette
        {
            get { return laRecette; }
            set { laRecette = value; }
        }

        public Recette LaRecetteAffichee
        {
            get { return _laRecetteAffichee; }
            set { _laRecetteAffichee = value; }
        }

        #endregion

        public frmConsulterRecette()
        {
            InitializeComponent();
        }

        private void frmConsulterRecette_Load(object sender, EventArgs e)
        {
            LaRecetteAffichee = new Recette();
            LaRecetteAffichee.Calories = LaRecette.Calories;
            LaRecetteAffichee.Nom = LaRecette.Nom;
            LaRecetteAffichee.Temps = LaRecette.Temps;
            LaRecetteAffichee.Description = LaRecette.Description;
            LaRecetteAffichee.Etapes = LaRecette.Etapes;
            LaRecetteAffichee.NbConvives = LaRecette.NbConvives;
            LaRecetteAffichee.Ingredients = LaRecette.Ingredients;
            if (!String.IsNullOrEmpty(LaRecetteAffichee.Nom))
                lblNomRecette.Text = LaRecetteAffichee.Nom;
            else lblNomRecette.Text = "La recette ne comporte aucun nom.";
            foreach (Ingredient ingredient in LaRecetteAffichee.Ingredients)
            {
                double qt = ingredient.QuantiteRequiseInitiale;
                ingredient.QuantiteRequise = qt;
                listboxIngredients.Items.Add($"{ingredient.Nom} | {ingredient.QuantiteRequiseInitiale * ingredient.MesureFixe} {ingredient.MesureUtilisee}");
                listboxIngredients.Items.Add("");
            }

            int i = 1;
            foreach (string etape in LaRecetteAffichee.Etapes)
            {
                listboxEtapes.Items.Add($"{i}. {etape}");
                listboxEtapes.Items.Add("");
                i++;
            }

            txtCalories.Text = Convert.ToString(LaRecetteAffichee.Calories);
            txtTemps.Text = LaRecetteAffichee.Temps;
            txtDescription.Text = LaRecetteAffichee.Description;
            if (LaRecette.CheminImage != "" && LaRecette.CheminImage != null)
            {
                picRecette.ImageLocation = LaRecette.CheminImage;
            }
        }

        #region Gestion des boutons

        private void btnRecalcul_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            LaRecetteAffichee.NbConvives = (int)numNbConvives.Value;
            LaRecetteAffichee.CalculerQuantitesEtCalories(true);
            laRecette.Nom = laRecette.Nom;
            listboxIngredients.Items.Clear();
            foreach (Ingredient ingredient in LaRecetteAffichee.Ingredients)
            {
                listboxIngredients.Items.Add($"{ingredient.Nom} | {ingredient.QuantiteRequise * ingredient.MesureFixe} {ingredient.MesureUtilisee}");
                listboxIngredients.Items.Add("");
            }
            txtCalories.Text = Convert.ToString(LaRecetteAffichee.Calories);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Owner.Show();
            this.Dispose();
        }

        #endregion

        #region Gestion de la fenêtre

        private void frmConsulterRecette_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner.Show();
            foreach (Ingredient ingredient in LaRecette.Ingredients)
            {
                ingredient.QuantiteRequise = ingredient.QuantiteRequiseInitiale;
            }
            this.Dispose();
        }

        #endregion
    }
}
