﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmHomePage : Form
    {
        #region Attributs

        private List<Ingredient> _listeIngredients;
        private frmLoginScreen _mainForm = null;
        private string _key;

        #endregion

        #region Accesseurs

        public string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        public List<Ingredient> ListeIngredients
        {
            get { return _listeIngredients; }
            set { _listeIngredients = value; }
        }

        public frmLoginScreen MainForm
        {
            get { return _mainForm; }
            set { _mainForm = value; }
        }

        #endregion

        public frmHomePage()
        {
            InitializeComponent();
        }

        public frmHomePage(Form callingForm, string pKey)
        {
            MainForm = callingForm as frmLoginScreen;
            Key = pKey;
            InitializeComponent();
            AfficherRecettes();
        }

        /// <summary>
        /// Cette méthode affiche les recettes de l'utilisateur dans la liste des recettes.
        /// </summary>
        public void AfficherRecettes()
        {
            listboxRecettes.Items.Clear();
            foreach (Recette recette in MainForm.DicoUsers[Key].Recettes)
            {
                listboxRecettes.Items.Add($"{recette.Nom} | {recette.Calories} calories | {recette.Temps}");
            }
        }

        #region Gestion des boutons

        private void btnCreerRecette_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Hide();
            frmCreerRecette frmCreerRecette = new frmCreerRecette(MainForm.ListeIngredients, this);
            frmCreerRecette.Owner = this;
            frmCreerRecette.Show();
        }

        private void btnConsulter_click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            if (listboxRecettes.SelectedIndex >= 0 && listboxRecettes.SelectedIndex < listboxRecettes.Items.Count)
            {
                this.Hide();
                frmConsulterRecette frmConsulterRecette = new frmConsulterRecette();
                frmConsulterRecette.Owner = this;
                frmConsulterRecette.LaRecette = MainForm.DicoUsers[Key].Recettes[listboxRecettes.SelectedIndex];
                frmConsulterRecette.Show();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Owner.Show();
            this.Dispose();
        }

        private void btnGererIngredients_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            frmAfficherIngredients frmAfficherIngredients = new frmAfficherIngredients(MainForm);
            frmAfficherIngredients.Owner = this;
            this.Hide();
            frmAfficherIngredients.Show();
        }

        private void btnClose_MouseEnter(object sender, EventArgs e)
        {
            btnClose.ForeColor = Color.FromArgb(0, 126, 249);
        }

        private void btnClose_MouseLeave(object sender, EventArgs e)
        {
            btnClose.ForeColor = Color.FromArgb(24, 30, 54);
        }

        private void btnSupprimerRecette_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            if (listboxRecettes.SelectedIndex >= 0 && listboxRecettes.SelectedIndex < listboxRecettes.Items.Count)
            {
                MainForm.DicoUsers[Key].Recettes.RemoveAt(listboxRecettes.SelectedIndex);
                AfficherRecettes();
            }
        }

        #endregion

        #region Gestion de la fenêtre

        private void frmHomePage_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Owner.Show();
        }

        #endregion

        /// <summary>
        /// Cette méthode crée une recette et l'ajoute à la liste des recettes avant
        /// de la sauvegarder pour l'utilisateur concerné.
        /// </summary>
        /// <param name="pRecette">La recette crée.</param>
        public void CreerRecette(Recette pRecette)
        {
            foreach (Ingredient ingredient in pRecette.Ingredients)
            {
                ingredient.QuantiteRequiseInitiale = ingredient.QuantiteRequise;
            }
            pRecette.CalculerQuantitesEtCalories();
            MainForm.DicoUsers[Key].Recettes.Add(pRecette);
            AfficherRecettes();
        }
    }
}
