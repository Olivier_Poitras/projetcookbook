﻿
namespace ProjetApprofondissement.Fenetres
{
    partial class frmCreerRecette
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreerRecette));
            this.txtNom = new System.Windows.Forms.TextBox();
            this.lblNom = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblNomRecette = new System.Windows.Forms.Label();
            this.txtTemps = new System.Windows.Forms.TextBox();
            this.lblTemps = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listboxEtapes = new System.Windows.Forms.ListBox();
            this.txtEtape = new System.Windows.Forms.TextBox();
            this.lblIngredients = new System.Windows.Forms.Label();
            this.lstIngredients = new System.Windows.Forms.ListBox();
            this.btnCreer = new System.Windows.Forms.Button();
            this.btnMoins = new System.Windows.Forms.Button();
            this.btnPlus = new System.Windows.Forms.Button();
            this.btnAjouterEtape = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnImportImage = new System.Windows.Forms.Button();
            this.lblCheminImage = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNom
            // 
            this.txtNom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.txtNom.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.txtNom.ForeColor = System.Drawing.Color.White;
            this.txtNom.Location = new System.Drawing.Point(224, 279);
            this.txtNom.Margin = new System.Windows.Forms.Padding(4);
            this.txtNom.Name = "txtNom";
            this.txtNom.Size = new System.Drawing.Size(439, 31);
            this.txtNom.TabIndex = 23;
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNom.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblNom.Location = new System.Drawing.Point(216, 235);
            this.lblNom.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(357, 42);
            this.lblNom.TabIndex = 22;
            this.lblNom.Text = "Nom de la recette :";
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.txtDescription.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.txtDescription.ForeColor = System.Drawing.Color.White;
            this.txtDescription.Location = new System.Drawing.Point(224, 389);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(4);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescription.Size = new System.Drawing.Size(439, 152);
            this.txtDescription.TabIndex = 25;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblDescription.Location = new System.Drawing.Point(216, 345);
            this.lblDescription.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(249, 42);
            this.lblDescription.TabIndex = 24;
            this.lblDescription.Text = "Description :";
            // 
            // lblNomRecette
            // 
            this.lblNomRecette.AutoSize = true;
            this.lblNomRecette.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.lblNomRecette.Font = new System.Drawing.Font("Lucida Bright", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomRecette.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblNomRecette.Location = new System.Drawing.Point(667, 0);
            this.lblNomRecette.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNomRecette.Name = "lblNomRecette";
            this.lblNomRecette.Size = new System.Drawing.Size(482, 53);
            this.lblNomRecette.TabIndex = 26;
            this.lblNomRecette.Text = "Création de recette";
            this.lblNomRecette.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtTemps
            // 
            this.txtTemps.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.txtTemps.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.txtTemps.ForeColor = System.Drawing.Color.White;
            this.txtTemps.Location = new System.Drawing.Point(224, 678);
            this.txtTemps.Margin = new System.Windows.Forms.Padding(4);
            this.txtTemps.Name = "txtTemps";
            this.txtTemps.Size = new System.Drawing.Size(439, 31);
            this.txtTemps.TabIndex = 28;
            // 
            // lblTemps
            // 
            this.lblTemps.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTemps.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblTemps.Location = new System.Drawing.Point(216, 578);
            this.lblTemps.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTemps.Name = "lblTemps";
            this.lblTemps.Size = new System.Drawing.Size(467, 86);
            this.lblTemps.TabIndex = 27;
            this.lblTemps.Text = "Temps nécessaire pour compléter la recette :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.label1.Location = new System.Drawing.Point(1173, 235);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(351, 42);
            this.label1.TabIndex = 31;
            this.label1.Text = "Étapes de cuisine :";
            // 
            // listboxEtapes
            // 
            this.listboxEtapes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.listboxEtapes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listboxEtapes.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.listboxEtapes.ForeColor = System.Drawing.Color.White;
            this.listboxEtapes.FormattingEnabled = true;
            this.listboxEtapes.HorizontalScrollbar = true;
            this.listboxEtapes.ItemHeight = 23;
            this.listboxEtapes.Location = new System.Drawing.Point(1181, 279);
            this.listboxEtapes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listboxEtapes.Name = "listboxEtapes";
            this.listboxEtapes.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.listboxEtapes.Size = new System.Drawing.Size(639, 345);
            this.listboxEtapes.TabIndex = 32;
            // 
            // txtEtape
            // 
            this.txtEtape.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.txtEtape.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.txtEtape.ForeColor = System.Drawing.Color.White;
            this.txtEtape.Location = new System.Drawing.Point(1181, 650);
            this.txtEtape.Margin = new System.Windows.Forms.Padding(4);
            this.txtEtape.Multiline = true;
            this.txtEtape.Name = "txtEtape";
            this.txtEtape.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEtape.Size = new System.Drawing.Size(540, 184);
            this.txtEtape.TabIndex = 33;
            // 
            // lblIngredients
            // 
            this.lblIngredients.AutoSize = true;
            this.lblIngredients.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIngredients.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblIngredients.Location = new System.Drawing.Point(703, 235);
            this.lblIngredients.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblIngredients.Name = "lblIngredients";
            this.lblIngredients.Size = new System.Drawing.Size(244, 42);
            this.lblIngredients.TabIndex = 35;
            this.lblIngredients.Text = "Ingrédients :";
            // 
            // lstIngredients
            // 
            this.lstIngredients.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.lstIngredients.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lstIngredients.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold);
            this.lstIngredients.ForeColor = System.Drawing.Color.White;
            this.lstIngredients.FormattingEnabled = true;
            this.lstIngredients.HorizontalScrollbar = true;
            this.lstIngredients.ItemHeight = 23;
            this.lstIngredients.Location = new System.Drawing.Point(711, 279);
            this.lstIngredients.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.lstIngredients.Name = "lstIngredients";
            this.lstIngredients.Size = new System.Drawing.Size(431, 437);
            this.lstIngredients.TabIndex = 36;
            // 
            // btnCreer
            // 
            this.btnCreer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.btnCreer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreer.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCreer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreer.Font = new System.Drawing.Font("Lucida Bright", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreer.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnCreer.Location = new System.Drawing.Point(1276, 0);
            this.btnCreer.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreer.Name = "btnCreer";
            this.btnCreer.Size = new System.Drawing.Size(143, 63);
            this.btnCreer.TabIndex = 37;
            this.btnCreer.Text = "Créer";
            this.btnCreer.UseVisualStyleBackColor = false;
            this.btnCreer.Click += new System.EventHandler(this.btnCreer_Click);
            // 
            // btnMoins
            // 
            this.btnMoins.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMoins.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMoins.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnMoins.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoins.Image = global::ProjetApprofondissement.Properties.Resources.remove72;
            this.btnMoins.Location = new System.Drawing.Point(828, 729);
            this.btnMoins.Margin = new System.Windows.Forms.Padding(4);
            this.btnMoins.Name = "btnMoins";
            this.btnMoins.Size = new System.Drawing.Size(96, 89);
            this.btnMoins.TabIndex = 39;
            this.btnMoins.TabStop = false;
            this.btnMoins.Tag = "Center";
            this.btnMoins.UseVisualStyleBackColor = true;
            this.btnMoins.Click += new System.EventHandler(this.btnMoins_Click);
            // 
            // btnPlus
            // 
            this.btnPlus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPlus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPlus.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnPlus.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlus.Image = global::ProjetApprofondissement.Properties.Resources.add72;
            this.btnPlus.Location = new System.Drawing.Point(932, 729);
            this.btnPlus.Margin = new System.Windows.Forms.Padding(4);
            this.btnPlus.Name = "btnPlus";
            this.btnPlus.Size = new System.Drawing.Size(96, 89);
            this.btnPlus.TabIndex = 38;
            this.btnPlus.TabStop = false;
            this.btnPlus.Tag = "Center";
            this.btnPlus.UseVisualStyleBackColor = true;
            this.btnPlus.Click += new System.EventHandler(this.btnPlus_Click);
            // 
            // btnAjouterEtape
            // 
            this.btnAjouterEtape.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAjouterEtape.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAjouterEtape.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnAjouterEtape.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterEtape.Image = global::ProjetApprofondissement.Properties.Resources.add72;
            this.btnAjouterEtape.Location = new System.Drawing.Point(1731, 650);
            this.btnAjouterEtape.Margin = new System.Windows.Forms.Padding(4);
            this.btnAjouterEtape.Name = "btnAjouterEtape";
            this.btnAjouterEtape.Size = new System.Drawing.Size(88, 185);
            this.btnAjouterEtape.TabIndex = 34;
            this.btnAjouterEtape.TabStop = false;
            this.btnAjouterEtape.Tag = "Center";
            this.btnAjouterEtape.UseVisualStyleBackColor = true;
            this.btnAjouterEtape.Click += new System.EventHandler(this.btnAjouterEtape_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.pictureBox3.Location = new System.Drawing.Point(1161, 223);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(4, 615);
            this.pictureBox3.TabIndex = 30;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.pictureBox1.Location = new System.Drawing.Point(691, 223);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(4, 615);
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Image = global::ProjetApprofondissement.Properties.Resources.back32;
            this.btnClose.Location = new System.Drawing.Point(16, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(44, 41);
            this.btnClose.TabIndex = 20;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.pictureBox2.Location = new System.Drawing.Point(-7, -4);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(4000, 66);
            this.pictureBox2.TabIndex = 19;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "rectangleDarkRed";
            // 
            // btnImportImage
            // 
            this.btnImportImage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.btnImportImage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImportImage.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnImportImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImportImage.Font = new System.Drawing.Font("Lucida Bright", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportImage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnImportImage.Location = new System.Drawing.Point(224, 740);
            this.btnImportImage.Margin = new System.Windows.Forms.Padding(4);
            this.btnImportImage.Name = "btnImportImage";
            this.btnImportImage.Size = new System.Drawing.Size(439, 63);
            this.btnImportImage.TabIndex = 45;
            this.btnImportImage.Text = "Importer une image";
            this.btnImportImage.UseVisualStyleBackColor = false;
            this.btnImportImage.Click += new System.EventHandler(this.btnImportImage_Click);
            // 
            // lblCheminImage
            // 
            this.lblCheminImage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCheminImage.Font = new System.Drawing.Font("Copperplate Gothic Light", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheminImage.ForeColor = System.Drawing.Color.White;
            this.lblCheminImage.Location = new System.Drawing.Point(56, 817);
            this.lblCheminImage.Name = "lblCheminImage";
            this.lblCheminImage.Size = new System.Drawing.Size(618, 45);
            this.lblCheminImage.TabIndex = 46;
            this.lblCheminImage.Text = "Aucune image sélectionnée.";
            // 
            // frmCreerRecette
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(73)))));
            this.ClientSize = new System.Drawing.Size(1942, 991);
            this.Controls.Add(this.lblCheminImage);
            this.Controls.Add(this.btnImportImage);
            this.Controls.Add(this.btnMoins);
            this.Controls.Add(this.btnPlus);
            this.Controls.Add(this.btnCreer);
            this.Controls.Add(this.lstIngredients);
            this.Controls.Add(this.lblIngredients);
            this.Controls.Add(this.btnAjouterEtape);
            this.Controls.Add(this.txtEtape);
            this.Controls.Add(this.listboxEtapes);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtTemps);
            this.Controls.Add(this.lblTemps);
            this.Controls.Add(this.lblNomRecette);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.txtNom);
            this.Controls.Add(this.lblNom);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pictureBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCreerRecette";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmCreerRecette";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCreerRecette_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox txtNom;
        private System.Windows.Forms.Label lblNom;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblNomRecette;
        private System.Windows.Forms.TextBox txtTemps;
        private System.Windows.Forms.Label lblTemps;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listboxEtapes;
        private System.Windows.Forms.TextBox txtEtape;
        private System.Windows.Forms.Button btnAjouterEtape;
        private System.Windows.Forms.Label lblIngredients;
        private System.Windows.Forms.ListBox lstIngredients;
        private System.Windows.Forms.Button btnCreer;
        private System.Windows.Forms.Button btnPlus;
        private System.Windows.Forms.Button btnMoins;
        private System.Windows.Forms.Button btnImportImage;
        private System.Windows.Forms.Label lblCheminImage;
    }
}