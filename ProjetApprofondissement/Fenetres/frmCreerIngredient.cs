﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmCreerIngredient : Form
    {
        #region Attributs

        private string _nomIngredient = "";
        private string _mesureFixe = "";
        private string _mesureUtilisee = "";
        private string _calories = "";

        #endregion

        #region Accesseurs

        public string NomIngredient
        {
            get { return _nomIngredient; }
            set { _nomIngredient = value; }
        }

        public string MesureFixe
        {
            get { return _mesureFixe; }
            set { _mesureFixe = value; }
        }

        public string MesureUtilisee
        {
            get { return _mesureUtilisee; }
            set { _mesureUtilisee = value; }
        }

        public string Calories
        {
            get { return _calories; }
            set { _calories = value; }
        }

        #endregion

        private frmAfficherIngredients _IngredientsForm = null;

        public frmAfficherIngredients IngredientsForm
        {
            get { return _IngredientsForm; }
            set { _IngredientsForm = value; }
        }

        public frmCreerIngredient()
        {
            InitializeComponent();
        }

        public frmCreerIngredient(Form callingForm)
        {
            IngredientsForm = callingForm as frmAfficherIngredients;
            InitializeComponent();
            txtApercu.Text = "";
        }

        #region Gestion des boutons

        private void btnCreer_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            int nbErreurs = 0;
            errorProvider1.Clear();

            int iTempo;
            bool isInt = false;
            isInt = int.TryParse(txtCalories.Text, out iTempo);
            if (!isInt)
            {
                nbErreurs++;
                errorProvider1.SetError(txtCalories, "Doit être un entier.");
            }
            else if (Convert.ToInt32(txtCalories.Text) <= 0 || Convert.ToInt32(txtCalories.Text) > 10000)
            {
                nbErreurs++;
                errorProvider1.SetError(txtCalories, "Doit être un entier entre 1 et 9999 inclusivement.");
            }

            int iTempo2;
            bool isInt2 = false;
            isInt2 = int.TryParse(txtMesureFixe.Text, out iTempo2);
            if (!isInt2)
            {
                nbErreurs++;
                errorProvider1.SetError(txtMesureFixe, "Doit être un entier.");
            }
            else if (Convert.ToInt32(txtMesureFixe.Text) <= 0 || Convert.ToInt32(txtMesureFixe.Text) > 10000)
            {
                nbErreurs++;
                errorProvider1.SetError(txtMesureFixe, "Doit être un entier entre 1 et 9999 inclusivement.");
            }

            if (txtNom.Text == " " || txtNom.Text == "" || txtNom.Text == null)
            {
                nbErreurs++;
                errorProvider1.SetError(txtNom, "Le nom de l'ingrédient est obligatoire.");
            }
            else if (txtNom.Text.Length > 50)
            {
                nbErreurs++;
                errorProvider1.SetError(txtNom, "Le nom de l'ingrédient ne peut pas dépasser 50 caractères.");
            }

            if (txtMesureUtilisee.Text.Length > 20)
            {
                nbErreurs++;
                errorProvider1.SetError(txtMesureUtilisee, "La mesure utilisée ne peut pas dépasser 20 caractères.");
            }

            if (nbErreurs == 0)
            {
                Ingredient monIngredient = new Ingredient(txtNom.Text, txtMesureUtilisee.Text,
                    Convert.ToInt32(txtMesureFixe.Text), Convert.ToInt32(txtCalories.Text));
                IngredientsForm.CreerIngredient(monIngredient);
                this.Owner.Show();
                this.Dispose();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Owner.Show();
            this.Dispose();
        }

        #endregion

        #region Gestion de l'aperçu

        private void txtNom_TextChanged(object sender, EventArgs e)
        {
            NomIngredient = txtNom.Text;
            txtApercu.Text = $"{NomIngredient} | {MesureFixe} {MesureUtilisee} | {Calories} calories.";
        }

        private void txtMesureFixe_TextChanged(object sender, EventArgs e)
        {
            MesureFixe = txtMesureFixe.Text;
            txtApercu.Text = $"{NomIngredient} | {MesureFixe} {MesureUtilisee} | {Calories} calories.";
        }

        private void txtMesureUtilisee_TextChanged(object sender, EventArgs e)
        {
            MesureUtilisee = txtMesureUtilisee.Text;
            txtApercu.Text = $"{NomIngredient} | {MesureFixe} {MesureUtilisee} | {Calories} calories.";
        }

        private void txtCalories_TextChanged(object sender, EventArgs e)
        {
            Calories = txtCalories.Text;
            txtApercu.Text = $"{NomIngredient} | {MesureFixe} {MesureUtilisee} | {Calories} calories.";
        }

        #endregion
    }
}
