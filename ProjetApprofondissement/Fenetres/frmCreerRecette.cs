﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace ProjetApprofondissement.Fenetres
{
    public partial class frmCreerRecette : Form
    {
        #region Attributs

        private List<Ingredient> _lstIngredients;
        private List<Ingredient> _lstIngredientsAffiches;
        private frmHomePage _homeForm = null;
        private List<string> _lstEtapes;
        private string _cheminImage;
        private string _cheminDossierImages;
        private bool _aUneImage = false;

        #endregion

        #region Accesseurs

        public List<Ingredient> LstIngredientsAffiches
        {
            get { return _lstIngredientsAffiches; }
            set { _lstIngredientsAffiches = value; }
        }

        public List<Ingredient> LstIngredients
        {
            get { return _lstIngredients; }
            set { _lstIngredients = value; }
        }

        public frmHomePage HomeForm
        {
            get { return _homeForm; }
            set { _homeForm = value; }
        }

        public List<string> LstEtapes
        {
            get { return _lstEtapes; }
            set { _lstEtapes = value; }
        }

        #endregion

        public frmCreerRecette()
        {
            InitializeComponent();
        }

        public frmCreerRecette(List<Ingredient> pLstIngredients, Form callingForm)
        {
            LstIngredients = pLstIngredients;
            LstEtapes = new List<string>();
            HomeForm = callingForm as frmHomePage;
            LstIngredientsAffiches = new List<Ingredient>();
            InitializeComponent();
            string currentDirectory = Directory.GetCurrentDirectory();
            string[] directoryBrise = currentDirectory.Split('\\');
            string directoryImages = "";
            foreach (string directory in directoryBrise)
            {
                string directoryAChanger = "non";
                if (directory == "bin")
                    directoryAChanger = "Autres\\";
                else if (directory == "Debug")
                    directoryAChanger = "ImagesRecettes\\";
                if (directoryAChanger == "non")
                {
                    directoryImages += directory;
                    directoryImages += "\\";
                }
                else
                {
                    directoryImages += directoryAChanger;
                }
            }

            _cheminDossierImages = directoryImages;
        }

        #region Gestion des boutons

        private void btnAjouterEtape_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            if (txtEtape.Text != "")
            {
                listboxEtapes.Items.Add(txtEtape.Text);
                LstEtapes.Add(txtEtape.Text);
                txtEtape.Clear();
            }
        }

        private void btnCreer_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Hide();
            Recette maRecette = new Recette();
            maRecette.NbConvives = 1;
            maRecette.Nom = txtNom.Text;
            maRecette.Description = txtDescription.Text;
            maRecette.Temps = txtTemps.Text;
            maRecette.Etapes = LstEtapes;
            maRecette.Ingredients = new List<Ingredient>();
            foreach (Ingredient ingredient in LstIngredientsAffiches)
            {
                if (ingredient.QuantiteRequise != 0)
                {
                    maRecette.Ingredients.Add(ingredient);
                }
            }

            if (_aUneImage)
            {
                File.Copy(_cheminImage, Path.Combine(_cheminDossierImages, Path.GetFileName(_cheminImage)), true);
                string[] directoryImageBrise = _cheminImage.Split('\\');
                string nomImage = directoryImageBrise[directoryImageBrise.Length - 1];
                maRecette.CheminImage = _cheminDossierImages + nomImage;
            }
            else
            {
                maRecette.CheminImage = "";
            }
            this.HomeForm.CreerRecette(maRecette);
            this.Owner.Show();
            this.Dispose();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            this.Owner.Show();
            foreach (Ingredient ingredient in LstIngredients)
            {
                ingredient.QuantiteRequise = 1;
            }
            this.Dispose();
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            if (lstIngredients.SelectedIndex >= 0 && lstIngredients.SelectedIndex < lstIngredients.Items.Count)
            {
                if (LstIngredientsAffiches[lstIngredients.SelectedIndex].QuantiteRequise <= 100)
                {
                    LstIngredientsAffiches[lstIngredients.SelectedIndex].QuantiteRequise++;
                    int i = lstIngredients.SelectedIndex;
                    lstIngredients.Items.Clear();
                    foreach (Ingredient ingredient in LstIngredientsAffiches)
                    {
                        lstIngredients.Items.Add($"{ingredient.Nom} ({ingredient.QuantiteRequise * ingredient.MesureFixe} {ingredient.MesureUtilisee})");
                    }
                    lstIngredients.SelectedIndex = i;
                }
            }
        }

        private void btnMoins_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            if (lstIngredients.SelectedIndex >= 0 && lstIngredients.SelectedIndex < lstIngredients.Items.Count)
            {
                if (LstIngredientsAffiches[lstIngredients.SelectedIndex].QuantiteRequise != 0)
                {
                    LstIngredientsAffiches[lstIngredients.SelectedIndex].QuantiteRequise--;
                    int i = lstIngredients.SelectedIndex;
                    lstIngredients.Items.Clear();
                    foreach (Ingredient ingredient in LstIngredientsAffiches)
                    {
                        lstIngredients.Items.Add($"{ingredient.Nom} ({ingredient.QuantiteRequise * ingredient.MesureFixe} {ingredient.MesureUtilisee})");
                    }
                    lstIngredients.SelectedIndex = i;
                }
            }
        }

        private void btnImportImage_Click(object sender, EventArgs e)
        {
            SoundPlayer splayer = new SoundPlayer(ProjetApprofondissement.Properties.Resources.ES_Button_Push_7___SFX_Producer);
            splayer.Play();
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp;)|*.jpg; *.jpeg; *.gif; *.bmp;";
            if (open.ShowDialog() == DialogResult.OK)
            {
                _cheminImage = open.FileName;
                lblCheminImage.Text = _cheminImage;
                _aUneImage = true;
            }
        }

        #endregion

        #region Gestion de la fenêtre

        private void frmCreerRecette_Load(object sender, EventArgs e)
        {
            List<Ingredient> lesIngredientsRajoutes = new List<Ingredient>();
            foreach (Ingredient ingredient in LstIngredients)
            {
                ingredient.QuantiteRequise = 0;
                bool estDuplique = false;
                if (lesIngredientsRajoutes.Count > 0)
                {
                    foreach (Ingredient ingredientRajoute in lesIngredientsRajoutes)
                    {
                        if (ingredient == ingredientRajoute || ingredient.Nom == ingredientRajoute.Nom)
                            estDuplique = true;
                    }
                }
                if (!estDuplique)
                {
                    lstIngredients.Items.Add($"{ingredient.Nom} ({ingredient.QuantiteRequise * ingredient.MesureFixe} {ingredient.MesureUtilisee})");
                    LstIngredientsAffiches.Add(ingredient);
                }
                lesIngredientsRajoutes.Add(ingredient);
            }
        }

        #endregion
    }
}
