﻿
namespace ProjetApprofondissement.Fenetres
{
    partial class frmHomePage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHomePage));
            this.lblRecettes = new System.Windows.Forms.Label();
            this.listboxRecettes = new System.Windows.Forms.ListBox();
            this.btnGererIngredients = new System.Windows.Forms.Button();
            this.btnConsulterRecette = new System.Windows.Forms.Button();
            this.btnSupprimerRecette = new System.Windows.Forms.Button();
            this.btnCreerRecette = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // lblRecettes
            // 
            this.lblRecettes.AutoSize = true;
            this.lblRecettes.Font = new System.Drawing.Font("Lucida Bright", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecettes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.lblRecettes.Location = new System.Drawing.Point(352, 76);
            this.lblRecettes.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRecettes.Name = "lblRecettes";
            this.lblRecettes.Size = new System.Drawing.Size(325, 54);
            this.lblRecettes.TabIndex = 1;
            this.lblRecettes.Tag = "Center";
            this.lblRecettes.Text = "Mes recettes :";
            // 
            // listboxRecettes
            // 
            this.listboxRecettes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.listboxRecettes.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.listboxRecettes.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listboxRecettes.ForeColor = System.Drawing.Color.White;
            this.listboxRecettes.FormattingEnabled = true;
            this.listboxRecettes.ItemHeight = 23;
            this.listboxRecettes.Location = new System.Drawing.Point(173, 132);
            this.listboxRecettes.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listboxRecettes.Name = "listboxRecettes";
            this.listboxRecettes.Size = new System.Drawing.Size(603, 506);
            this.listboxRecettes.TabIndex = 3;
            this.listboxRecettes.Tag = "Center";
            // 
            // btnGererIngredients
            // 
            this.btnGererIngredients.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(79)))), ((int)(((byte)(99)))));
            this.btnGererIngredients.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGererIngredients.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGererIngredients.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGererIngredients.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(126)))), ((int)(((byte)(249)))));
            this.btnGererIngredients.Location = new System.Drawing.Point(668, 669);
            this.btnGererIngredients.Margin = new System.Windows.Forms.Padding(4);
            this.btnGererIngredients.Name = "btnGererIngredients";
            this.btnGererIngredients.Size = new System.Drawing.Size(341, 84);
            this.btnGererIngredients.TabIndex = 8;
            this.btnGererIngredients.TabStop = false;
            this.btnGererIngredients.Tag = "Center";
            this.btnGererIngredients.Text = "Gérer les ingrédients";
            this.btnGererIngredients.UseVisualStyleBackColor = false;
            this.btnGererIngredients.Click += new System.EventHandler(this.btnGererIngredients_Click);
            // 
            // btnConsulterRecette
            // 
            this.btnConsulterRecette.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnConsulterRecette.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnConsulterRecette.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsulterRecette.Image = global::ProjetApprofondissement.Properties.Resources.eye72;
            this.btnConsulterRecette.Location = new System.Drawing.Point(783, 320);
            this.btnConsulterRecette.Margin = new System.Windows.Forms.Padding(4);
            this.btnConsulterRecette.Name = "btnConsulterRecette";
            this.btnConsulterRecette.Size = new System.Drawing.Size(104, 96);
            this.btnConsulterRecette.TabIndex = 19;
            this.btnConsulterRecette.TabStop = false;
            this.btnConsulterRecette.Tag = "Center";
            this.btnConsulterRecette.UseVisualStyleBackColor = true;
            this.btnConsulterRecette.Click += new System.EventHandler(this.btnConsulter_click);
            // 
            // btnSupprimerRecette
            // 
            this.btnSupprimerRecette.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSupprimerRecette.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSupprimerRecette.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupprimerRecette.Image = global::ProjetApprofondissement.Properties.Resources.delete72;
            this.btnSupprimerRecette.Location = new System.Drawing.Point(783, 423);
            this.btnSupprimerRecette.Margin = new System.Windows.Forms.Padding(4);
            this.btnSupprimerRecette.Name = "btnSupprimerRecette";
            this.btnSupprimerRecette.Size = new System.Drawing.Size(104, 96);
            this.btnSupprimerRecette.TabIndex = 18;
            this.btnSupprimerRecette.TabStop = false;
            this.btnSupprimerRecette.Tag = "Center";
            this.btnSupprimerRecette.UseVisualStyleBackColor = true;
            this.btnSupprimerRecette.Click += new System.EventHandler(this.btnSupprimerRecette_Click);
            // 
            // btnCreerRecette
            // 
            this.btnCreerRecette.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCreerRecette.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCreerRecette.Font = new System.Drawing.Font("Kristen ITC", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreerRecette.Image = global::ProjetApprofondissement.Properties.Resources.add72;
            this.btnCreerRecette.Location = new System.Drawing.Point(783, 526);
            this.btnCreerRecette.Margin = new System.Windows.Forms.Padding(4);
            this.btnCreerRecette.Name = "btnCreerRecette";
            this.btnCreerRecette.Size = new System.Drawing.Size(104, 96);
            this.btnCreerRecette.TabIndex = 17;
            this.btnCreerRecette.TabStop = false;
            this.btnCreerRecette.Tag = "Center";
            this.btnCreerRecette.UseVisualStyleBackColor = true;
            this.btnCreerRecette.Click += new System.EventHandler(this.btnCreerRecette_click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.btnClose.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.ForeColor = System.Drawing.Color.Black;
            this.btnClose.Image = global::ProjetApprofondissement.Properties.Resources.home32;
            this.btnClose.Location = new System.Drawing.Point(16, 11);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(44, 41);
            this.btnClose.TabIndex = 9;
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            this.btnClose.MouseEnter += new System.EventHandler(this.btnClose_MouseEnter);
            this.btnClose.MouseLeave += new System.EventHandler(this.btnClose_MouseLeave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(30)))), ((int)(((byte)(54)))));
            this.pictureBox2.Location = new System.Drawing.Point(-9, -2);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(4000, 65);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Tag = "rectangleDarkRed";
            // 
            // frmHomePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(51)))), ((int)(((byte)(73)))));
            this.ClientSize = new System.Drawing.Size(1040, 780);
            this.Controls.Add(this.btnConsulterRecette);
            this.Controls.Add(this.btnSupprimerRecette);
            this.Controls.Add(this.btnCreerRecette);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnGererIngredients);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.listboxRecettes);
            this.Controls.Add(this.lblRecettes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "frmHomePage";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HomePage";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHomePage_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRecettes;
        private System.Windows.Forms.ListBox listboxRecettes;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button btnGererIngredients;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnSupprimerRecette;
        private System.Windows.Forms.Button btnCreerRecette;
        private System.Windows.Forms.Button btnConsulterRecette;
    }
}