﻿
namespace ProjetApprofondissement
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txtIdentifiantRecuperer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIdentifiantCreer = new System.Windows.Forms.TextBox();
            this.lblNomRecette1 = new System.Windows.Forms.Label();
            this.lblNomIngredient2Recette1 = new System.Windows.Forms.Label();
            this.btnTestDictionnaires = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Copperplate Gothic Light", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(19, 115);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(411, 204);
            this.button1.TabIndex = 0;
            this.button1.Text = "Sauvegarder un utilisateur.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnTest_click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Copperplate Gothic Light", 25.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(436, 115);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(411, 204);
            this.button2.TabIndex = 1;
            this.button2.Text = "Récupérer l\'utilisateur";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btnTest2_click);
            // 
            // txtIdentifiantRecuperer
            // 
            this.txtIdentifiantRecuperer.Location = new System.Drawing.Point(436, 58);
            this.txtIdentifiantRecuperer.Name = "txtIdentifiantRecuperer";
            this.txtIdentifiantRecuperer.Size = new System.Drawing.Size(211, 22);
            this.txtIdentifiantRecuperer.TabIndex = 2;
            this.txtIdentifiantRecuperer.Text = "identifiant";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(457, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Votre identifiant:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(33, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Votre identifiant:";
            // 
            // txtIdentifiantCreer
            // 
            this.txtIdentifiantCreer.Location = new System.Drawing.Point(12, 58);
            this.txtIdentifiantCreer.Name = "txtIdentifiantCreer";
            this.txtIdentifiantCreer.Size = new System.Drawing.Size(211, 22);
            this.txtIdentifiantCreer.TabIndex = 4;
            this.txtIdentifiantCreer.Text = "identifiant";
            // 
            // lblNomRecette1
            // 
            this.lblNomRecette1.AutoSize = true;
            this.lblNomRecette1.Location = new System.Drawing.Point(490, 364);
            this.lblNomRecette1.Name = "lblNomRecette1";
            this.lblNomRecette1.Size = new System.Drawing.Size(46, 17);
            this.lblNomRecette1.TabIndex = 6;
            this.lblNomRecette1.Text = "label3";
            // 
            // lblNomIngredient2Recette1
            // 
            this.lblNomIngredient2Recette1.AutoSize = true;
            this.lblNomIngredient2Recette1.Location = new System.Drawing.Point(611, 364);
            this.lblNomIngredient2Recette1.Name = "lblNomIngredient2Recette1";
            this.lblNomIngredient2Recette1.Size = new System.Drawing.Size(46, 17);
            this.lblNomIngredient2Recette1.TabIndex = 7;
            this.lblNomIngredient2Recette1.Text = "label4";
            // 
            // btnTestDictionnaires
            // 
            this.btnTestDictionnaires.Location = new System.Drawing.Point(130, 409);
            this.btnTestDictionnaires.Name = "btnTestDictionnaires";
            this.btnTestDictionnaires.Size = new System.Drawing.Size(175, 61);
            this.btnTestDictionnaires.TabIndex = 8;
            this.btnTestDictionnaires.Text = "Tester les dictionnaires";
            this.btnTestDictionnaires.UseVisualStyleBackColor = true;
            this.btnTestDictionnaires.Click += new System.EventHandler(this.btnTestDictionnaires_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 548);
            this.Controls.Add(this.btnTestDictionnaires);
            this.Controls.Add(this.lblNomIngredient2Recette1);
            this.Controls.Add(this.lblNomRecette1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtIdentifiantCreer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtIdentifiantRecuperer);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtIdentifiantRecuperer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtIdentifiantCreer;
        private System.Windows.Forms.Label lblNomRecette1;
        private System.Windows.Forms.Label lblNomIngredient2Recette1;
        private System.Windows.Forms.Button btnTestDictionnaires;
    }
}

