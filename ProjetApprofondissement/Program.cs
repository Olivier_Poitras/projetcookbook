﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjetApprofondissement.Fenetres;

namespace ProjetApprofondissement
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmLoginScreen());
        } // Fin du static void Main()
    } // Fin de class Program

    public class DataSerializer
    {
        public void JsonSerialize(object data, string filePath)
        {
            JsonSerializer jsonSerializer = new JsonSerializer();
            if (File.Exists(filePath)) File.Delete(filePath);
            StreamWriter sWriter = new StreamWriter(filePath);
            JsonWriter jsonWriter = new JsonTextWriter(sWriter);
            jsonSerializer.Serialize(jsonWriter, data);
            jsonWriter.Close();
            sWriter.Close();
        }

        public object JsonDeserialize(Type dataType, string filePath)
        {
            JObject obj = null;
            List<Ingredient> objs = null;
            JsonSerializer jsonSerializer = new JsonSerializer();
            if (File.Exists(filePath))
            {
                StreamReader sReader = new StreamReader(filePath);
                JsonReader jsonReader = new JsonTextReader(sReader);
                if (dataType == typeof(List<Ingredient>))
                {
                    string json = sReader.ReadToEnd();
                    objs = JsonConvert.DeserializeObject<List<Ingredient>>(json);
                }
                else obj = jsonSerializer.Deserialize(jsonReader) as JObject;
                jsonReader.Close();
                sReader.Close();
            }

            if (dataType == typeof(List<Ingredient>))
                return objs;
            return obj.ToObject(dataType);
        }
    }

} // Fin du namespace
