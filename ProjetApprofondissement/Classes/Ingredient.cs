using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Remoting.Lifetime;
using System.Text;
using System.Threading.Tasks;

namespace ProjetApprofondissement
{
    public class Ingredient
    {
        #region Attributs

        private string _nom;

        // Note : La quantité requise est initialement à 1. Elle sera remplacée lorsque l'utilisateur
        // va créer une recette. Il va alors préciser la quantité requise (X) de la mesure fixe (Y).
        // Par exemple: (En créant une recette) Je veux 4 fois 100 ml d'eau = Je veux 400 ml d'eau.
        // C'est donc un string qui contiendra: $"{QuantiteRequise} * {MesureFixe} + {MesureUtilisee}".
        private double _quantiteRequise;

        // Note : Elle sert à se souvenir de la quantité requise initialement voulue
        // lorsqu'on modifie la quantité requise pour l'affichage.
        private double _quantiteRequiseInitiale;

        // Note : lorsqu'on précisera la mesure utilisée, l'utilisateur doit préciser une
        // mesure de cuisine comme des grammes, des tasses ou des ml.
        private string _mesureUtilisee;

        // Note: lorsqu'on précisera la mesure fixe, c'est l'unité qui sera utilisée pour calculer
        // les calories de cet ingrédient. Par exemple, pour de la farine, on voudra avoir :
        // MesureFixe = 100, MesureUtilisee = "grammes", Calories = 364.
        // Ensuite, l'utilisateur pourra modifier la quantite requise (au moment de créer une recette).
        private int _mesureFixe;

        // Note : selon la mesure utilisée, les calories devraient être une quantité uniforme.
        // Par exemple, on pourrait dire que pour les choses en grammes, c'est toujours X calories
        // par 100 grammes de l'ingrédient. X calories par 10 ml. Ainsi, lorsqu'on veut calculer
        // les calories, on pourra faire : QuantiteRequise * Calories.
        private int _calories;

        #endregion

        #region Accesseurs

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public double QuantiteRequise
        {
            get { return _quantiteRequise; }
            set { _quantiteRequise = value; }
        }

        public double QuantiteRequiseInitiale
        {
            get { return _quantiteRequiseInitiale; }
            set { _quantiteRequiseInitiale = value; }
        }

        public string MesureUtilisee
        {
            get { return _mesureUtilisee; }
            set { _mesureUtilisee = value; }
        }

        public int MesureFixe
        {
            get { return _mesureFixe; }
            set { _mesureFixe = value; }
        }

        public int Calories
        {
            get { return _calories; }
            set { _calories = value; }
        }

        #endregion

        #region Constructeurs

        /// <summary>
        /// Au moment de créer un ingrédient, il faut absolument avoir un nom, une mesure
        /// utilisée (par exemple, des grammes), une mesure fixe (par exemple, 100 (grammes))
        /// et le nombre de calories pour cette mesure fixe. Ensuite, la quantité requise de cet ingrédient
        /// est initialisé à 1 et sera modifié par l'utilisateur au moment de créer une recette.
        /// </summary>
        /// <param name="pNom">Le nom de l'ingrédient.</param>
        /// <param name="pMesureUtilisee">La mesure utilisée.</param>
        /// <param name="pMesureFixe">La quantité de la mesure utilisée pour représenter une instance de l'ingrédient.</param>
        /// <param name="pCalories">Le nombre de calories par instance de la mesure fixe. Ainsi, la quantité requise de l'ingrédient
        /// * les calories = total calories pour cet ingrédient au sein d'une recette.</param>
        public Ingredient(string pNom, string pMesureUtilisee, int pMesureFixe, int pCalories)
        {
            Nom = pNom;
            MesureUtilisee = pMesureUtilisee;
            MesureFixe = pMesureFixe;
            Calories = pCalories;
            QuantiteRequise = 1;
        }

        /// <summary>
        /// Constructeur vide.
        /// </summary>
        public Ingredient()
        {
        }

        #endregion

        #region Methodes

        /// <summary>
        /// Calcule le nombre de convives * la quantité requise de cet ingrédient dans la recette * le
        /// nombre de calories pour une instance de cet ingrédient.
        /// </summary>
        /// <param name="pNbConvives">Le nombre de convives pour la recette.</param>
        /// <returns>Retourne le nombre de calories totales pour cet ingrédient au sein de la recette selon
        /// le nombre de convives.</returns>
        public double CalculerCalories()
        {
            double caloriesTotales = 0;
            caloriesTotales += Calories * QuantiteRequise;
            return caloriesTotales;
        }

        #endregion
    } // Fin de class Ingredient
} // Fin du namespace
