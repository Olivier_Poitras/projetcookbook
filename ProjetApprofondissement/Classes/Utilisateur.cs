﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjetApprofondissement
{
    public class Utilisateur
    {
        #region Attributs

        private string _identifiant;
        private byte[] _motDePasse;
        private List<Recette> _recettes;

        #endregion

        #region Accesseurs

        public string Identifiant
        {
            get { return _identifiant; }
            set { _identifiant = value; }
        }

        public byte[] MotDePasse
        {
            get { return _motDePasse; }
            set { _motDePasse = value; }
        }

        public List<Recette> Recettes
        {
            get { return _recettes; }
            set { _recettes = value; }
        }

        #endregion

        #region Constructeurs

        /// <summary>
        /// Constructeur qui initialise la liste des recettes. Au moment de créer un utilisateur,
        /// il doit toujours y avoir un identifiant et un mot de passe. On initialise aussi le chemin
        /// du fichier propre à l'utilisateur.
        /// </summary>
        /// <param name="pIdentifiant"></param>
        /// <param name="pMotDePasse"></param>
        public Utilisateur(string pIdentifiant, byte[] pMotDePasse)
        {
            Identifiant = pIdentifiant;
            MotDePasse = pMotDePasse;
            //CheminDossier = $"../Utilisateurs/{pIdentifiant}";
            //CheminDossier = "C:\\Users\\poitr\\OneDrive\\Documents\\Cégep Garneau\\Session 3\\Projet d'approfondissement en programmation\\Mon projet\\ProjetApprofondissement\\ProjetApprofondissement\\Utilisateurs"; 
            //"C: \Users\poitr\OneDrive\Documents\Cégep Garneau\Session 3\Projet d'approfondissement en programmation\Mon projet\ProjetApprofondissement\ProjetApprofondissement\Utilisateurs"
            //CheminDossier = $"{pIdentifiant}.save";
            Recettes = new List<Recette>();
        }

        /// <summary>
        /// Constructeur vide.
        /// </summary>
        public Utilisateur()
        {
        }

        #endregion
    } // Fin de class Utilisateur
} // Fin du namsepace
