﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ProjetApprofondissement
{
    public class Recette
    {
        #region Attributs

        private List<Ingredient> _ingredients;
        private string _nom;
        private string _description;
        private double _calories;
        private string _temps;
        private List<string> _etapes;
        private int _nbConvives;
        private string _cheminImage;

        #endregion

        #region Accesseurs

        public List<Ingredient> Ingredients
        {
            get { return _ingredients; }
            set { _ingredients = value; }
        }

        public string CheminImage
        {
            get { return _cheminImage; }
            set { _cheminImage = value; }
        }

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public double Calories
        {
            get { return _calories; }
            set { _calories = value; }
        }

        public string Temps
        {
            get { return _temps; }
            set { _temps = value; }
        }

        public List<string> Etapes
        {
            get { return _etapes; }
            set { _etapes = value; }
        }

        public int NbConvives
        {
            get { return _nbConvives; }
            set { _nbConvives = value; }
        }

        #endregion

        #region Constructeurs

        /// <summary>
        /// Au moment de créer une recette, on a seuleument besoin du nom, de la description
        /// et du temps de préparation requis. Le reste sera ajouté ultérieurement.
        /// On initialise donc les quatres autres attributs.
        /// </summary>
        /// <param name="pNom"></param>
        /// <param name="pTemps"></param>
        /// <param name="pDescription"></param>
        public Recette(string pNom, string pDescription, string pTemps)
        {
            Nom = pNom;
            Description = pDescription;
            Temps = pTemps;
            Etapes = new List<string>();
            Ingredients = new List<Ingredient>();
            NbConvives = 1;
            Calories = 0;
        }

        /// <summary>
        /// Constructeur vide
        /// </summary>
        public Recette()
        {
        }

        #endregion

        #region Methodes

        /// <summary>
        /// Cette méthode calcule les quantités et calories d'un ingrédient.
        /// </summary>
        public void CalculerQuantitesEtCalories()
        {
            Calories = 0;
            foreach (Ingredient ingredient in Ingredients)
            {
                ingredient.QuantiteRequise = ingredient.QuantiteRequise * NbConvives;
                Calories += ingredient.CalculerCalories();
            }
        }

        /// <summary>
        /// Cette méthode calcule les quantités et calories d'un ingrédient sans le modifier.
        /// Elle est utilisée dans le cas d'un recalcul seulement à l'affichage.
        /// </summary>
        /// <param name="pAffichage"></param>
        public void CalculerQuantitesEtCalories(bool pAffichage)
        {
            if (pAffichage)
            { 
                Calories = 0;
                foreach (Ingredient ingredient in Ingredients)
                {
                    ingredient.QuantiteRequise = ingredient.QuantiteRequiseInitiale * NbConvives;
                    Calories += ingredient.CalculerCalories();
                }
            }
        }

        #endregion
    } // Fin de class Recette
} // Fin du namespace
