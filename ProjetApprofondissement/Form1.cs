﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProjetApprofondissement.Fenetres;

namespace ProjetApprofondissement
{
    public partial class Form1 : Form
    {
        #region Attributs

        private frmLoginScreen _mainForm = null;

        #endregion

        #region Accesseurs

        public frmLoginScreen MainForm
        {
            get { return _mainForm; }
            set { _mainForm = value; }
        }

        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        public Form1(Form callingForm)
        {
            MainForm = callingForm as frmLoginScreen;
            InitializeComponent();
        }

        private void btnTest_click(object sender, EventArgs e)
        {
            //Utilisateur unUtilisateur = new Utilisateur("monIdentifiant", "monMotDePasse");
            //Recette uneRecette = new Recette("Jambalaya", "Du riz, des saucisses et du poulet.", "2 heures");
            //Ingredient unIngredient = new Ingredient("Riz", "grammes", 100, 156);
            //uneRecette.Ingredients.Add(unIngredient);
            //uneRecette.Ingredients[0].QuantiteRequise = 3;
            //Ingredient unIngredient2 = new Ingredient("Saucisse", "unités", 1, 100);
            //uneRecette.Ingredients.Add(unIngredient2);
            //uneRecette.Ingredients[1].QuantiteRequise = 2;
            //uneRecette.CalculerQuantitesEtCalories();
            //unUtilisateur.Recettes.Add(uneRecette);

            //DataSerializer dataSerializer = new DataSerializer();
            //dataSerializer.JsonSerialize(unUtilisateur, unUtilisateur.CheminDossier);

            //Utilisateur unUtilisateur2 = new Utilisateur(txtIdentifiantCreer.Text, "monMotDePasse");
            //Recette uneRecette3 = new Recette("Biscuit", "Un biscuit de farine et oeufs.", "2 heures");
            //Ingredient unIngredient3 = new Ingredient("Farine", "grammes", 100, 95);
            //uneRecette3.Ingredients.Add(unIngredient3);
            //uneRecette3.Ingredients[0].QuantiteRequise = 4;
            //Ingredient unIngredient4 = new Ingredient("Oeuf", "unités", 1, 150);
            //uneRecette3.Ingredients.Add(unIngredient4);
            //uneRecette3.Ingredients[1].QuantiteRequise = 2;
            //uneRecette3.CalculerQuantitesEtCalories();
            //unUtilisateur2.Recettes.Add(uneRecette3);

            //DataSerializer dataSerializer2 = new DataSerializer();
            //dataSerializer2.JsonSerialize(unUtilisateur2, unUtilisateur2.CheminDossier);

        }

        private void btnTest2_click(object sender, EventArgs e)
        {
            //Utilisateur unUtilisateurRecupere = new Utilisateur(txtIdentifiantRecuperer.Text, "monMotDePasse");
            //DataSerializer dataSerializer = new DataSerializer();
            //unUtilisateurRecupere = dataSerializer.JsonDeserialize(typeof(Utilisateur), unUtilisateurRecupere.CheminDossier) as Utilisateur;
            //lblNomRecette1.Text = unUtilisateurRecupere.Recettes[0].Nom;
            //lblNomIngredient2Recette1.Text = unUtilisateurRecupere.Recettes[0].Ingredients[1].Nom;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnTestDictionnaires_Click(object sender, EventArgs e)
        {
            MainForm.ListeIngredients.Add(new Ingredient("Riz", "grammes", 100, 156));
            MainForm.ListeIngredients.Add(new Ingredient("Farine", "grammes", 100, 95));
            MainForm.ListeIngredients.Add(new Ingredient("Oeuf", "unités", 1, 150));
        }
    }
}
